working_directory="$1"
sql_directory=$working_directory/sql_files/

#overwrite current readme file to include substitution string contained in template file
cp $working_directory/readme.template $working_directory/readme.md

#extract all lines containing marker '--@' from sql files in sql directory to temporary file
for f in $sql_directory/*.sql
do
        echo "## [$(basename -- $f)](./sql_files/$(basename -- $f))" >> $sql_directory/temp.sql
				awk '/--@/' "$f" >> $sql_directory/temp.sql
        echo -e "\n\n" >> $sql_directory/temp.sql
done

#remove leading and trailing whitespace, then add two spaces to end of each line as markdown newline formatting
awk '{$1=$1}1' $sql_directory/temp.sql | sed 's/$/  /' > $sql_directory/formatted_temp.sql

#replace substitution string with contents of formatted temporary file
sed -e "/{sql_files}/ {" -e "r "$sql_directory"/formatted_temp.sql" -e "d" -e "}" -i $working_directory/readme.md

#cleanup temp sql files
rm $sql_directory/*temp*.sql

#push new readme to git
git add $working_directory/.
git commit -m "Updating Readme $(date)"
git push origin master

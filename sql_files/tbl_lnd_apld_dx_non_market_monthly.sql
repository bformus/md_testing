/*
--@ Lands apld_dx_non_market_monthly data from S3 bucket to landing schema.
*/

-- this grabs apld_dx_non_market_monthly data
copy into landing.tbl_lnd_apld_dx_non_market_monthly
  from @s3_stage/apld_dx_non_market_monthly/
  file_format = (format_name = sf_parquet_ingestion)
	pattern='.+\.parquet'
  on_error = 'skip_file'
;

# APLD

SQL Files - Landing:
## [tbl_lnd_apld_dx_market_monthly.sql](./sql_files/tbl_lnd_apld_dx_market_monthly.sql)  
--@ Lands apld_dx_market_monthly data from S3 bucket to landing schema.  
  
  
  
## [tbl_lnd_apld_dx_non_market_monthly.sql](./sql_files/tbl_lnd_apld_dx_non_market_monthly.sql)  
--@ Lands apld_dx_non_market_monthly data from S3 bucket to landing schema.  
  
  
  
## [tbl_lnd_apld_px_market_monthly.sql](./sql_files/tbl_lnd_apld_px_market_monthly.sql)  
--@ Lands apld_px_market_monthly data from S3 bucket to landing schema.  
  
  
  
## [tbl_lnd_apld_px_non_market_monthly.sql](./sql_files/tbl_lnd_apld_px_non_market_monthly.sql)  
--@ Lands apld_px_non_market_monthly data from S3 bucket to landing schema.  
  
  
  
## [tbl_lnd_apld_rx_market_monthly.sql](./sql_files/tbl_lnd_apld_rx_market_monthly.sql)  
--@ Lands apld_rx_market_monthly data from S3 bucket to landing schema.  
  
  
  
## [tbl_lnd_apld_rx_non_market_monthly.sql](./sql_files/tbl_lnd_apld_rx_non_market_monthly.sql)  
--@ Lands apld_rx_non_market_monthly data from S3 bucket to landing schema.  
  
  
  


